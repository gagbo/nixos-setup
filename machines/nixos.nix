# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let
  unstableTarball =
    fetchTarball
      https://github.com/NixOS/nixpkgs-channels/archive/nixos-unstable-small.tar.gz;
in
{
  imports =
    [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  nixpkgs.config = {
    allowUnfree = false;

    packageOverrides = pkgs: {
      unstable = import unstableTarball {
        # Import the allowUnfree setting from above
        config = config.nixpkgs.config;
      };
    };
  };

  # Use the GRUB 2 boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.grub = {
    enable = true;
    version = 2;
    efiSupport = true;
    efiInstallAsRemovable = true;
    gfxmodeEfi = "1024x768";
    # Define on which hard drive you want to install Grub.
    device = "nodev"; # or "nodev" for efi only
  };
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";

  networking.hostName = "NixVM"; # Define your hostname.
  networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  system.copySystemConfiguration = true; # copy system configuration

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "fr_FR.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Paris";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # Utilities / dev packages
    home-manager
    source-code-pro dejavu_fonts
    wget curl git yadm
    bc ripgrep exa fd
    sudo
    python37Full
    cmake gnumake
    blueman

    # Shell
    fish tmux

    # Editors
    neovim
    (emacs.override { withGTK3 = true;
    withGTK2 = false; })

    # UI stuff
    plasma5.ksshaskpass
    xorg.xrdb xorg.setxkbmap xorg.xmodmap
    i3
    rofi compton dunst
    stalonetray xss-lock
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };
  programs.fish.enable = true;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # XServer setup
  services.xserver = {
      # Enable the X11 windowing system.
      enable = true;
      layout = "us";
      xkbVariant = "altgr-intl";
      xkbOptions = "eurosign:e, ctrl:nocaps, shift:both_capslock";

      # Enable touchpad support.
      libinput.enable = true;

      # Enable SDDM
      displayManager.sddm.enable = true;

      # Add a basic startx session
      displayManager.session = [
        { manage = "desktop";
        name = "Xinit-compat";
        start =
          ''
        #!/usr/bin/env sh
        for session in ~/.xsession ~/.Xclients /etc/X11/xinit/Xclients ;
        do
        if [ -f ''${session} ] ; then
            exec ''${session}
        fi
        done
          '';
        }
      ];

      # Enable the KDE Desktop Environment.
      desktopManager.plasma5.enable = true;
    };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.mutableUsers = true;
  users.users.gagbo = {
    isNormalUser = true;
    uid = 1000;
    home = "/home/gagbo";
    description = "Gerry Agbobada";
    extraGroups = [ "wheel" "networkmanager" ];
    shell = pkgs.fish;
    initialPassword = "test";
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.10"; # Did you read the comment?

}
