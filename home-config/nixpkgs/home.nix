{ pkgs, stdenv, fetchurl, ... }:

let home_directory  = builtins.getEnv "HOME";
dotfiles_repo_url   = "https://framagit.org/gagbo/bare-dots.git";
unstable = import <unstable> {};
dmenu-gagbo = pkgs.dmenu.overrideAttrs ( oldAttrs: rec {
  src = pkgs.fetchurl {
    url = "https://github.com/gagbo/dmenu/archive/v1.0.tar.gz";
    sha256 = "17s9fckhwwvfr1nxmwlxp9mklyxc76gajhxf8q4gb0iivw0vpn5f";
  };
  preConfigure = ''
    sed -i "s@PREFIX = .*@PREFIX = $out@g" config.mk
  '';
  meta = with pkgs.stdenv.lib; {
    description = "Gagbo's fork of dmenu";
    longDescription = ''
      Check the repo on https://github.com/gagbo/dmenu for information about
      patches applied
    '';
    homepage = https://github.com/gagbo/dmenu;
    license = licenses.mit;
    maintainers = [ pkgs.maintainers.gagbo ];
    platforms = platforms.all;
  };
}); in

{
  home.packages = with pkgs; [
    firefox qutebrowser keepassxc
    owncloud
    vlc_qt5 mpd ncmpcpp youtube-dl
    neofetch conky feh imagemagick scrot
    kitty alacritty
    fzf fd ripgrep exa
    universal-ctags global

    # UI stuff for all WM/DEs
    dmenu-gagbo jq lm_sensors
    xfce.xfce4-power-manager
    wireless-tools

    # QTile
    unstable.qtile unstable.weather-icons python37Packages.mpd2
    python37Packages.dbus-python python37Packages.pygobject3
  ];

  programs.fzf = {
    enable = true;
      # defaultCommand = "fd --type f";
      # changeDirWidgetCommand = "fd --type d";
      # changeDirWidgetOptions = [ "--preview 'exa -T {} | head -200'" ];
      # fileWidgetCommand = "fd --type f";
    };

    home.file = {
      check_or_clone_dotfiles = {
        target = "init_dotfiles_profile.sh";
        executable = true;
        text =
        ''
            #!/usr/bin/env sh
            yadm "${dotfiles_repo_url}"
        '';
      };
    };

    systemd.user.startServices = true;
  }
